<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Demande extends Model
{
    use softDeletes;
    protected $fillable = ['objet', 'club_id', 'local', 'date', 'from', 'to', 'position', 'desc', 'cmnt'];
    
    public function logDemandes(){
        return $this->hasMany('App\LogDemande');
    }
}
