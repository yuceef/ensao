<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Demande;
use App\LogDemande;
use Validator;
 
class DemandeController extends APIBaseController
{
    public function __construct()
    {
        //$this->middleware('level:Club')->only('store');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $demandes = Demande::all();
        //return $this->sendResponse($demandes->toArray(), 'Posts retrieved successfully.');
    }

    public function myDemande(Request $request)
    {
        $id = $request->user()->id;
        $level = $request->user()->level;
        if($level == "Ade" or $level == "Club")$demandes["club"] = Demande::where('club_id',$id)->orderBy('id','desc')->get();
        if($level == "Ade" or $level == "Administration")$demandes["admin"] = Demande::where('position','<=',$request->user()->position)->orderBy('id','desc')->get();    
        return $this->sendResponse($demandes, 'Posts retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->user()->level=="Club" or $request->user()->level=="Ade"){
            $input = $request->all();
            $validator = Validator::make($input, [
                'objet' => 'required|string|max:120',
                'local' => 'required|string|max:120',
                'date' => 'required|date',
                'from' => 'required',
                'to' => 'required',
                'desc' => 'required|string|min:10'
            ]);
            if($validator->fails()){
                return $this->sendError('Les erreurs suivantes:', $validator->errors());       
            }
            $input['club_id'] = $request->user()->id;
            $resultat = Demande::create($input);
            return $this->sendResponse($resultat->toArray(), 'La demande a été envoyer.');
        }
        return $this->sendError('Vous n\'avez pas le droit.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $deamande = Demande::find($id);
        if (is_null($deamande)) {
            return $this->sendError('Demande not found.');
        }
        if ($request->user()->id == $deamande->club_id or ($request->user()->level !='Club' and $request->user()->position >= $deamande->position)) {
            $deamande->logDemandes;
            return $this->sendResponse($deamande->toArray(), 'Demande retrieved successfully.');
        }
        return $this->sendError('Vous n\'avez pas le droit.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deamande = Demande::find($id);
        if (is_null($deamande)) {
            return $this->sendError('Demande not found.');
        }
        if($request->get('isResp')){
            $inputEdit = [
                'demande_id' => $id,
                'user_id' => $request->user()->id,
                'action' => $request->get('typeRes'),
            ];
            if($request->get('typeRes')=='Accept')$deamande->position = $request->user()->position - 1;
            else{
                $deamande->position = 6;
                $inputEdit['cmnt'] = $request->get('cmnt');
            }
            LogDemande::create($inputEdit);
            $deamande->save();
            return $this->sendResponse($deamande->toArray(), 'La demande a été bien modifier.');            
        }
        if($deamande->position == 6 and $deamande->club_id == $request->user()->id ){
            $input = $request->all();
            $validator = Validator::make($input, [
                'objet' => 'required|string|max:120',
                'local' => 'required|string|max:120',
                'date' => 'required|date',
                'desc' => 'required|string|min:10'
            ]);
            if($validator->fails()){
                return $this->sendError('Les erreurs suivantes:', $validator->errors());       
            }
            $deamande->objet = $input['objet'];
            $deamande->local = $input['local'];
            $deamande->date = $input['date'];
            $deamande->from = $input['from'];
            $deamande->to = $input['to'];
            $deamande->position--;
            $deamande->desc = $input['desc'];
            $deamande->cmnt = $input['cmnt'];
            $deamande->save();
            $inputEdit = [
                'demande_id' => $id,
                'user_id' => $request->user()->id,
                'action' => 'Modifier',
            ];
            LogDemande::create($inputEdit);
            return $this->sendResponse($deamande->toArray(), 'La demande a été bien modifier.');
        }
        return $this->sendError('Vous n\'avez pas le droit.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $deamande = Demande::find($id);
        if (is_null($deamande)) {
            return $this->sendError('User not found.');
        }
        if($deamande->club_id == $request->user()->id ){
            $objet = $deamande->objet;
            Demande::destroy($id);
            return $this->sendResponse($objet, 'La demande a été supprimer.');
        }
        return $this->sendError('Vous n\'avez pas le droit.');    
    }
}
