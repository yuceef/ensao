<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogDemande extends Model
{
    protected $fillable = ['demande_id', 'user_id', 'action', 'cmnt'];
}
