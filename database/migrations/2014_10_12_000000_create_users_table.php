<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('num')->nullable();
            $table->string('password');
            $table->enum('level',['Administration','Club','Ade']);
            $table->integer('position');
            $table->boolean('isActive')->default(true);
            $table->text('desc')->nullable();
            $table->string('logo')->nullable();
            $table->string('president')->nullable();
            $table->string('vice_president')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
