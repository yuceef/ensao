<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_demandes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('demande_id');
            $table->integer('user_id');
            $table->enum('action',['Refuse','Accept','Modifier']);
            $table->string('cmnt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_demandes');
    }
}
