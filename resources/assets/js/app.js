// app.js

import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import DemandesEnCours from './components/Demandes/DemandesEnCours.vue';
import DemandeCreate from './components/Demandes/DemandeCreate.vue';
import Demande from './components/Demandes/Demande.vue';
import DemandeEdit from './components/Demandes/DemandeEdit.vue';
import Demandes from './components/Demandes/Demandes.vue';
import UserCreate from './components/UserCreate.vue';
import Securite from './components/Securite.vue';
import Example from './components/Example.vue';

const routes = [
    {
        name: 'Home',
        path: '/',
        component: Example
    },
    {
        name: 'DemandeCreate',
        path: '/demande/create',
        component: DemandeCreate
    },
    {
        name: 'Demandes',
        path: '/demandes',
        component: Demandes
    },
    {
        name: 'DemandeEdit',
        path: '/demande/edit/:id',
        component: DemandeEdit
    },
    {
        name: 'Demande',
        path: '/demande/:id',
        component: Demande
    },
    {
        name: 'DemandesEnCours',
        path: '/demandesEnCours',
        component: DemandesEnCours
    },
    {
        name: 'UserCreate',
        path: '/user/create',
        component: UserCreate
    },
    {
        name: 'Securite',
        path: '/user/securite',
        component: Securite
    },
];

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount('#app');