<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="bg-dark">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
        <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
            <div class="container aside-xxl">
              <a class="navbar-brand block" href="index.html">ENSA d'Oujda</a>
              <section class="panel panel-default bg-white m-t-lg">
                <header class="panel-heading text-center">
                    <strong>Se Connecter</strong>
              </header>
              <form  method="POST" action="{{ route('login') }}" class="panel-body wrapper-lg">
                  {{ csrf_field() }}
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="control-label">Email</label>
                    <input type="text" class="form-control input-lg" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>          
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label">Mot de Passe</label>
                    <input type="password" class="form-control input-lg" name="password" required >
                    @if ($errors->has('password'))
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Me Remembre
                    </label>
                </div>
                <a href="#" class="pull-right m-t-xs"><small>Mot de pass oblier?</small></a>
                <button type="submit" class="btn btn-primary">Se Connecter</button>
                <div class="line line-dashed"></div>
            </form>
        </section>
    </div>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder">
        <p>
            <small>Yuceef &copy; ENSAO Parascolaire 2018</small>
        </p>
    </div>
</footer>
</div>
</body>
</html>
