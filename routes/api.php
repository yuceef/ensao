<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/** @return user */
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth');

/**  Demandes */
Route::get('demandes/my', 'API\DemandeController@myDemande')->middleware('auth');
Route::resource('logdemandes', 'API\LogDemandeController')->middleware('auth');
Route::resource('demandes', 'API\DemandeController')->middleware('auth');
Route::resource('users', 'API\UserController')->middleware('auth');
